package com.example.phsyren.Movie4Today.HTTP;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by Phsyren on 2016-03-11.
 */
public class HttpConnect {

    private static final OkHttpClient client = new OkHttpClient();

    public static String run(String[] url) throws IOException, JSONException {

        Request request = new Request.Builder()
                .url(url[0])
                .tag("OkHttpConnect")
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {

                if (!response.isSuccessful()) {
                    throw new IOException("Inexpected code: " + response);
                }

            }

        });

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
