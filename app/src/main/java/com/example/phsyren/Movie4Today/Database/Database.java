package com.example.phsyren.Movie4Today.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Phsyren on 2016-03-21.
 */
public class Database extends SQLiteOpenHelper {

    public static final String MOVIE_DB = "MOVIE_DB.db";
    public static final int VERSION = 1;

    public static final String CREATE_MOVIE_DB =
            "create table " + DatabaseContract.Movie.TABLE_NAME
                    + " (" + DatabaseContract.Movie._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + DatabaseContract.Movie.MOVIE_ID + " TEXT, "
                    + DatabaseContract.Movie.ID_GENRES + " TEXT, "
                    + DatabaseContract.Movie.IMAGE + " TEXT, "
                    + DatabaseContract.Movie.TITLE + " TEXT, "
                    + DatabaseContract.Movie.RELEASE_DATE + " TEXT, "
                    + DatabaseContract.Movie.OVERVIEW + " TEXT, "
                    + DatabaseContract.Movie.VOTE_AVERAGE + " TEXT, "
                    + DatabaseContract.Movie.FAVORITE + " INTEGER, "
                    + DatabaseContract.Movie.isRate + " DOUBLE)";


    public static final String CREATE_GENRES_DB =
            "create table " + DatabaseContract.Genres.TABLE_NAME
                    + " (" + DatabaseContract.Genres._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + DatabaseContract.Genres.GENRES_ID + " INTEGER, "
                    + DatabaseContract.Genres.GENRES_NAME + " TEXT)";

    public Database(Context context) {
        super(context, MOVIE_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_MOVIE_DB);
        db.execSQL(CREATE_GENRES_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXIST " + DatabaseContract.Movie.TABLE_NAME);
        onCreate(db);
    }


}
