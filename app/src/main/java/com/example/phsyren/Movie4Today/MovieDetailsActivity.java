package com.example.phsyren.Movie4Today;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phsyren.Movie4Today.Database.DAO;
import com.example.phsyren.Movie4Today.Database.Database;
import com.example.phsyren.Movie4Today.Database.DatabaseContract;
import com.example.phsyren.Movie4Today.Model.Genres;
import com.example.phsyren.Movie4Today.Model.Movie;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Phsyren on 2016-03-12.
 */
public class MovieDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "extra_movie";

    private Movie movie;
    private Movie finalDbMovie;
    private DAO dao;
    Context context;
    private Database database;
    private ImageView icon;
    private TextView title;
    private TextView release_date;
    private TextView overview;
    private TextView genres;
    private TextView rate;
    private ImageButton rate_flbtn;
    private ImageButton fav_btn;

    public MovieDetailsActivity(Context context) {
        this.context = context;
    }

    public MovieDetailsActivity() {
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie_detail);

        dao = new DAO(getApplicationContext());
        database = new Database(getApplicationContext());

        movie = (Movie) getIntent().getSerializableExtra(EXTRA_MOVIE);

        new GetGenresFromDB(movie.genre_ids).execute();

        icon = (ImageView) findViewById(R.id.selected_item_imageView);
        title = (TextView) findViewById(R.id.movie_title);
        release_date = (TextView) findViewById(R.id.movie_release_date);
        genres = (TextView) findViewById(R.id.selected_item_genres_description);
        rate = (TextView) findViewById(R.id.selected_item_movieRate_rate);

        overview = (TextView) findViewById(R.id.movie_overview_container);

        Picasso.with(context).load("http://image.tmdb.org/t/p/w500" + movie.getPoster_path()).resize(130, 160).centerCrop().into(icon);
        title.setText(movie.getTitle());
        release_date.setText(movie.getRelease_date());
        rate.setText(movie.getVote_average() + "/10");
        overview.setText(movie.getOverview());

        rate_flbtn = (ImageButton) findViewById(R.id.floating_rate_button);
        fav_btn = (ImageButton) findViewById(R.id.floating_love_button);

        Movie dbMovie = null;
        try {
            dbMovie = dao.getMovieFromDB(movie.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (dbMovie != null) {
            setFavouriteButtonColor(dbMovie.getFavourite());
            setRateButtonColor(dbMovie.getUserRating());
        }

        rate_flbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRateDialog(movie);
            }
        });

        finalDbMovie = dbMovie;
        fav_btn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (finalDbMovie == null) {
                    try {
                        dao.insertMovie(movie);
                        movie.setFavourite(true);
                        setFavouriteButtonColor(true);
                        setColorMsg(true);
                        finalDbMovie = dao.getMovieFromDB(movie.getId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return;
                }
                boolean currentFavoriteState = !finalDbMovie.getFavourite();
                dao.updateFavourite(finalDbMovie.getId(), currentFavoriteState);
                setFavouriteButtonColor(currentFavoriteState);
                setColorMsg(currentFavoriteState);
                try {
                    finalDbMovie = dao.getMovieFromDB(movie.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showRateDialog(Movie movie) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        RateDialog rateDialog = RateDialog.newInstance(movie);
        rateDialog.show(fragmentManager, "rate_dialog");
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setFavouriteButtonColor(boolean isFavourite) {
        if (isFavourite) {
            fav_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        } else
            fav_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorUnLike)));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setRateButtonColor(float rate) {
        if (rate > 0) {
            rate_flbtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorRate)));
        } else
            rate_flbtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorUnRate)));
    }

    private void setColorMsg(boolean isFavourite) {
        if (isFavourite) {
            Toast.makeText(getApplicationContext(), "Dodano do ulubionych", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getApplicationContext(), "Usunięto z ulubionych", Toast.LENGTH_SHORT).show();
    }

    private void setRateMsg(boolean isRated) {
        if (isRated) {
            Toast.makeText(getApplicationContext(), "Dodano do oceniony", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getApplicationContext(), "Usunięto z ocenionych", Toast.LENGTH_SHORT).show();
    }

    public class GetGenresFromDB extends AsyncTask<ArrayList<Integer>, Void, String> {

        ArrayList<Integer> arrayList;
//        SQLiteDatabase sqLiteDatabase;
        String fullGenresName = "";

        public GetGenresFromDB(ArrayList<Integer> arrayList) {
            this.arrayList = arrayList;
        }

        @Override
        protected String doInBackground(ArrayList<Integer>... params) {

            Cursor cursor = database.getReadableDatabase()
                    .query(DatabaseContract.Genres.TABLE_NAME
                            , null, null, null, null, null, null);

            List<Genres> mGenres = dao.convertGenresToListFromCursor(cursor);

            cursor.close();
            database.getReadableDatabase().close();

            Map<Integer, String> mappedGenres = new HashMap<>();
            for (Genres genres : mGenres) {
                mappedGenres.put(genres.getId(), genres.getName());
            }

            for (int i = 0; i < arrayList.size(); i++) {
                int genresId = arrayList.get(i);
                String genresName = mappedGenres.get(genresId);
                if (i != arrayList.size() - 1) {
                    fullGenresName += genresName + ", ";
                } else fullGenresName += genresName;
            }

            return fullGenresName;
        }

        @Override
        protected void onPostExecute(String s) {

            genres.setText(fullGenresName);
        }
    }
}
