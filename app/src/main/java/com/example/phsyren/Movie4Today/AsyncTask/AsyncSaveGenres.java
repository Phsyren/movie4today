package com.example.phsyren.Movie4Today.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.example.phsyren.Movie4Today.Adapter.GenresAdapter;
import com.example.phsyren.Movie4Today.Database.DAO;
import com.example.phsyren.Movie4Today.Database.Database;
import com.example.phsyren.Movie4Today.HTTP.HttpConnect;
import com.example.phsyren.Movie4Today.Model.Genres;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-04-19.
 */
public class AsyncSaveGenres extends AsyncTask<String, Void, List<Genres>> {

    Context context;
    GenresAdapter genresAdapter;
    Database database;
    DAO dao;

    public AsyncSaveGenres(Context context, GenresAdapter genresAdapter) {
        this.context = context;
        this.genresAdapter = genresAdapter;
        database = new Database(context);
        dao = new DAO(context);
    }

    @Override
    protected List<Genres> doInBackground(String... params) {

        try {
            List<Genres> mGenres = new ArrayList<Genres>();

            String jsonData;
            jsonData = HttpConnect.run(params);
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONArray jsonArray = jsonObject.getJSONArray("genres");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                Genres genres = Genres.GenresfromJsonObject(object);
                dao.insertGenres(genres);
            }

            mGenres = Genres.GenresfromJsonArray(jsonArray);


            return mGenres;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Genres> mGenres) {
        super.onPostExecute(mGenres);

        genresAdapter.addAll(mGenres);
    }
}
