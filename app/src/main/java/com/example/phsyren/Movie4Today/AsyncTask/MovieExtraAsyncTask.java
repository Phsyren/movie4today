package com.example.phsyren.Movie4Today.AsyncTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.HTTP.HttpConnect;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-30.
 */
public abstract class MovieExtraAsyncTask extends AsyncTask<String, Void, List<Movie>> {

    public ProgressDialog progressDialog;
    protected MoviesAdapter moviesAdapter;

    RecyclerView recyclerView;
    static int total_pages = 2;
    int page;


    public MovieExtraAsyncTask(Context context, MoviesAdapter moviesAdapter, int page, RecyclerView recyclerView) {
        this.moviesAdapter = moviesAdapter;
        this.page = page;
        this.recyclerView = recyclerView;
        progressDialog = new ProgressDialog(context);
    }

    public abstract void setMoviesList(List<Movie> movies);


    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Download");
        progressDialog.show();
    }

    @Override
    protected List<Movie> doInBackground(String... params) {

        if (page <= total_pages) {

            try {
                List<Movie> movies = new ArrayList<Movie>();

                String jsonData;
                jsonData = HttpConnect.run(params);
                JSONObject jsonObject = new JSONObject(jsonData);
                JSONArray jsonArray = jsonObject.getJSONArray("results");

                total_pages = jsonObject.getInt("total_pages");

                movies = Movie.fromJsonArray(jsonArray);

                return movies;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;

    }


    @Override
    protected void onPostExecute(List<Movie> movies) {

        try {
            if ((this.progressDialog != null) && this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {

        } catch (final Exception e) {

        } finally {
            this.progressDialog = null;
        }

        if (recyclerView != null) {
            setMoviesList(movies);
        }


        return;
    }
}
