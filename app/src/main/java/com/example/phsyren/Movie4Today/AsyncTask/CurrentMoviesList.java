package com.example.phsyren.Movie4Today.AsyncTask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.Model.Movie;

import java.util.List;

/**
 * Created by Phsyren on 2016-04-01.
 */
public class CurrentMoviesList extends MovieExtraAsyncTask {


    public CurrentMoviesList(Context context, MoviesAdapter moviesAdapter, int page, RecyclerView recyclerView) {
        super(context, moviesAdapter, page, recyclerView);

    }

    @Override
    public void setMoviesList(List<Movie> movies) {
        moviesAdapter.addMovieList(movies);
        moviesAdapter.notifyDataSetChanged();
    }
}

