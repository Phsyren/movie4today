package com.example.phsyren.Movie4Today;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.Database.DAO;
import com.example.phsyren.Movie4Today.Database.Database;
import com.example.phsyren.Movie4Today.Database.DatabaseContract;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

public class FavouriteMovieFragment extends Fragment {

    MoviesAdapter moviesAdapter;
    DAO dao;
    RecyclerView recyclerView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favourite_movie, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dao = new DAO(getContext());

        moviesAdapter = new MoviesAdapter(getContext(), false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);


        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(moviesAdapter);

    }

    public class LoadFavouriteMovies extends AsyncTask<Void, Void, List<Movie>> {

        private Database database = new Database(getActivity());
        private DAO dao = new DAO(getContext());

        @Override
        protected List<Movie> doInBackground(Void... params) {

            final Cursor result = database
                    .getReadableDatabase()
                    .query(DatabaseContract.Movie.TABLE_NAME
                            , null
                            , DatabaseContract.Movie.FAVORITE + " = " + 1
                            , null, null, null, null);


            List<Movie> movies = null;
            try {
                movies = dao.convertMovieToListFromCursor(result);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            result.close();
            database.getReadableDatabase().close();

            return movies;
        }

        @Override
        protected void onPostExecute(List<Movie> movies) {
            moviesAdapter.clearAndAddMovieList(movies);
            moviesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadFavouriteMovies().execute();

    }
}
