package com.example.phsyren.Movie4Today.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.phsyren.Movie4Today.Model.Movie;
import com.example.phsyren.Movie4Today.MovieDetailsActivity;
import com.example.phsyren.Movie4Today.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-11.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    List<Movie> data = new ArrayList<>();
    Movie movie;
    private LayoutInflater layoutInflater;
    Context context;
    boolean showRate;

    public MoviesAdapter(Context context) {

        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public MoviesAdapter(Context context, boolean showRate) {

        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.showRate = showRate;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Movie currentMovie;

        public TextView movie_title;
        public TextView release_date;
        public TextView user_rate;
        public ImageView icon;


        public ViewHolder(View view) {
            super(view);

            movie_title = (TextView) view.findViewById(R.id.movie_title);
            release_date = (TextView) view.findViewById(R.id.release_date);
            user_rate = (TextView) view.findViewById(R.id.user_rate);
            icon = (ImageView) view.findViewById(R.id.image_movie_details);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(context, MovieDetailsActivity.class);
            intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE, currentMovie);
            context.startActivity(intent);
        }

    }

    public void addMovieList(List<Movie> movies) {
        data.addAll(movies);
    }

    public void clearAndAddMovieList(List<Movie> movies) {
        if (movies != null) {
            data.clear();
            addMovieList(movies);
            return;
        }
        data.clear();
    }

    public MoviesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_movie, parent, false);


        return new MoviesAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MoviesAdapter.ViewHolder holder, int position) {
        movie = data.get(position);
        holder.currentMovie = movie;


        Picasso.with(context).load("http://image.tmdb.org/t/p/w150" + movie.getPoster_path()).into(holder.icon);
        holder.movie_title.setText(movie.getTitle());
        holder.release_date.setText(movie.getRelease_date());
        holder.release_date.setTextSize(14);
        holder.user_rate.setText(String.valueOf(movie.getUserRating()) + "/6");


        if (showRate == true) {
            holder.user_rate.setVisibility(View.VISIBLE);
        } else
            holder.user_rate.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}

