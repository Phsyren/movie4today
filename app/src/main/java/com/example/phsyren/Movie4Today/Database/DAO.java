package com.example.phsyren.Movie4Today.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.Model.Genres;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-04-27.
 */
public class DAO {

    private Database database;
    private Context context;
    private Cursor cursor;
    private MoviesAdapter moviesAdapter;
    private Movie movie;
    private Genres genres;
    private String[] findMovieId = {DatabaseContract.Movie.MOVIE_ID};
    private String[] isFavourite = {DatabaseContract.Movie.FAVORITE};
    private String[] isRated = {DatabaseContract.Movie.MOVIE_ID, DatabaseContract.Movie.isRate};

    public DAO(Context context) {
        database = new Database(context);
        this.context = context;
    }

    public Movie converterMovieFromCursor(Cursor cursor) throws JSONException, ParseException {
        movie = new Movie();

        if (cursor.getCount() == 0) {
            return null;
        }

        final int idMovieColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.MOVIE_ID);
        final int idGenresColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.ID_GENRES);
        final int imageColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.IMAGE);
        final int titleColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.TITLE);
        final int dateColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.RELEASE_DATE);
        final int overviewColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.OVERVIEW);
        final int voteAverageColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.VOTE_AVERAGE);
        final int rateColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.isRate);
        final int favoriteColumnIndex = cursor.getColumnIndex(DatabaseContract.Movie.FAVORITE);

        JSONArray jsonArray = new JSONArray(cursor.getString(idGenresColumnIndex));

        movie.setGenre_ids(Movie.fromJsonArrayToArray(jsonArray));
        movie.setId(cursor.getString(idMovieColumnIndex));
        movie.setPoster_path(cursor.getString(imageColumnIndex));
        movie.setTitle(cursor.getString(titleColumnIndex));
        movie.setRelease_date(cursor.getString(dateColumnIndex));
        movie.setOverview(cursor.getString(overviewColumnIndex));
        movie.setVote_average(cursor.getString(voteAverageColumnIndex));
        movie.setUserRating(cursor.getFloat(rateColumnIndex));
        movie.setFavourite(cursor.getInt(favoriteColumnIndex) > 0);

        return movie;
    }

    public List<Movie> convertMovieToListFromCursor(Cursor cursor) throws JSONException, ParseException {
        List<Movie> movies = new ArrayList<Movie>();

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        do {
            movies.add(converterMovieFromCursor(cursor));
        } while (cursor.moveToNext());

        return movies;
    }

    public Genres converterGenresFromCursor(Cursor cursor) {
        genres = new Genres();

        if (cursor.getCount() == 0) {
            return null;
        }

        final int idGenresColumnIndex = cursor.getColumnIndex(DatabaseContract.Genres.GENRES_ID);
        final int genresNameColumnIndex = cursor.getColumnIndex(DatabaseContract.Genres.GENRES_NAME);

        genres.setId(cursor.getInt(idGenresColumnIndex));
        genres.setName(cursor.getString(genresNameColumnIndex));

        return genres;
    }

    public List<Genres> convertGenresToListFromCursor(Cursor cursor) {
        List<Genres> mGenres = new ArrayList<Genres>();
        cursor.moveToFirst();

        do {
            mGenres.add(converterGenresFromCursor(cursor));
        } while (cursor.moveToNext());

        return mGenres;
    }

    public void insertMovie(Movie movie) throws JSONException {

        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();

        sqLiteDatabase.beginTransaction();

        ContentValues contentValues = new ContentValues();

        String genresArray = new JSONArray(movie.getGenre_ids()).toString();

        contentValues.put(DatabaseContract.Movie.ID_GENRES, genresArray);

        contentValues.put(DatabaseContract.Movie.MOVIE_ID, movie.getId());
        contentValues.put(DatabaseContract.Movie.IMAGE, movie.getPoster_path());
        contentValues.put(DatabaseContract.Movie.TITLE, movie.getTitle());
        contentValues.put(DatabaseContract.Movie.RELEASE_DATE, movie.getRelease_date());
        contentValues.put(DatabaseContract.Movie.VOTE_AVERAGE, movie.getVote_average());
        contentValues.put(DatabaseContract.Movie.OVERVIEW, movie.getOverview());
        contentValues.put(DatabaseContract.Movie.isRate, movie.getUserRating());
        contentValues.put(DatabaseContract.Movie.FAVORITE, movie.getFavourite());

        sqLiteDatabase.insertOrThrow(DatabaseContract.Movie.TABLE_NAME, null, contentValues);

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void insertGenres(Genres genres) {

        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();


        sqLiteDatabase.beginTransaction();

        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseContract.Genres.GENRES_ID, genres.getId());
        contentValues.put(DatabaseContract.Genres.GENRES_NAME, genres.getName());

        sqLiteDatabase.insertOrThrow(DatabaseContract.Genres.TABLE_NAME, null, contentValues);

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public Movie getMovieFromDB(String id) throws JSONException, ParseException {
        Cursor cursor = database.getReadableDatabase()
                .query(DatabaseContract.Movie.TABLE_NAME
                        , null
                        , DatabaseContract.Movie.MOVIE_ID + " = " + id
                        , null, null, null, null);
        cursor.moveToFirst();
        Movie dbMovie;

        dbMovie = converterMovieFromCursor(cursor);


        cursor.close();
        database.getReadableDatabase().close();
        return dbMovie;
    }

    public List<Genres> getGenresFromDB() {

        Cursor cursor = database.getReadableDatabase()
                .query(DatabaseContract.Genres.TABLE_NAME
                        , null, null, null, null, null, null);

        List<Genres> mGenres = convertGenresToListFromCursor(cursor);
        cursor.close();
        database.getReadableDatabase().close();

        return mGenres;
    }

    public void updateFavourite(String id, boolean b) {

        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();

        sqLiteDatabase.beginTransaction();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.Movie.FAVORITE, b);

        sqLiteDatabase.update(DatabaseContract.Movie.TABLE_NAME, contentValues, DatabaseContract.Movie.MOVIE_ID + " = " + id, null);

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

    }

    public void updateRate(String id, double rate) {

        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();

        sqLiteDatabase.beginTransaction();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.Movie.isRate, rate);

        sqLiteDatabase.update(DatabaseContract.Movie.TABLE_NAME, contentValues, DatabaseContract.Movie.MOVIE_ID + " = " + id, null);

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

}
