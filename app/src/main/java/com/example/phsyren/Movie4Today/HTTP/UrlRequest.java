package com.example.phsyren.Movie4Today.HTTP;

/**
 * Created by Phsyren on 2016-03-16.
 */
public class UrlRequest {

    public static final String API_KEY = "api_key=6d6169eb545767e4865955fc68731c23";
    public static final String URL_API = "https://api.themoviedb.org/3/";

    public static final String URL_API_BEST_RATE = "https://api.themoviedb.org/3/movie/top_rated?api_key=6d6169eb545767e4865955fc68731c23&language=pl&page=";

    public static final String URL_API_QUERY = "http://api.themoviedb.org/3/search/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&query=";
    public static final String URL_API_PAGES = "&page=";

    public static final String URL_API_POPULAR = "https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc&page=";
    public static final String URL_API_BEST_RATE_MOVIEWS_FORM_GENRES = "discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=vote_average.desc&vote_count.gte=100&with_genres=>>28<<";


    public static final String URL_API_GENRES_LIST = "https://api.themoviedb.org/3/genre/list?api_key=6d6169eb545767e4865955fc68731c23&language=pl";
}
