package com.example.phsyren.Movie4Today.AsyncTask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.Model.Movie;

import java.util.List;

/**
 * Created by Phsyren on 2016-03-30.
 */
public class NewMoviesList extends MovieExtraAsyncTask {

    public NewMoviesList(Context context, MoviesAdapter moviesAdapter, int page, RecyclerView recyclerView) {
        super(context, moviesAdapter, page, recyclerView);
    }

    @Override
    public void setMoviesList(List<Movie> movies) {
        moviesAdapter.clearAndAddMovieList(movies);
        moviesAdapter.notifyDataSetChanged();
    }
}
