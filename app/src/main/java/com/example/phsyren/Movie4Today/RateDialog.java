package com.example.phsyren.Movie4Today;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.phsyren.Movie4Today.Database.DAO;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONException;

import java.text.ParseException;

/**
 * Created by Phsyren on 2016-04-26.
 */
public class RateDialog extends DialogFragment {

    private RatingBar ratingBar;
    private Button cancelBtn;
    private Button rateBtn;
    DAO dao;

    private Movie movie;


    public RateDialog() {

    }

    public static RateDialog newInstance(Movie movie) {
        RateDialog rateDialog = new RateDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("rateMovie", movie);
        rateDialog.setArguments(bundle);
        return rateDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rate_movie, container, false);

        ratingBar = (RatingBar) view.findViewById(R.id.rating_bar);
        cancelBtn = (Button) view.findViewById(R.id.cancel_btn);
        rateBtn = (Button) view.findViewById(R.id.rate_btn);

        dao = new DAO(getContext());

        movie = (Movie) getArguments().getSerializable("rateMovie");

        Movie dbMovie = null;
        try {
            dbMovie = dao.getMovieFromDB(movie.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (dbMovie != null) {
            ratingBar.setRating(dbMovie.getUserRating());
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateDialog.this.dismiss();
            }
        });

        final Movie finalDbMovie = dbMovie;
        rateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieDetailsActivity movieDetailsActivity = (MovieDetailsActivity) getActivity();
                if (finalDbMovie == null) {
                    movie.setUserRating(ratingBar.getRating());
                    try {
                        dao.insertMovie(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    movieDetailsActivity.setRateButtonColor(movie.getUserRating());
                    Toast.makeText(getActivity(), "Zapisano ocene", Toast.LENGTH_LONG).show();
                    RateDialog.this.dismiss();
                    return;
                }
                float rate = ratingBar.getRating();
                dao.updateRate(finalDbMovie.getId(), rate);
                movieDetailsActivity.setRateButtonColor(rate);
                Toast.makeText(getActivity(), "Zapisano ocene", Toast.LENGTH_LONG).show();
                RateDialog.this.dismiss();
            }
        });

        return view;
    }
}
