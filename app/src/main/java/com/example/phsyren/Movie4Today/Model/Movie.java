package com.example.phsyren.Movie4Today.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-11.
 */
public class Movie implements Serializable {

    public static final String MOVIE_ID = "id";
    public static final String TITLE = "title";
    public static final String IMAGE = "poster_path";
    public static final String RELEASE_DATE = "release_date";
    public static final String GENRES_ID = "genre_ids";
    public static final String OVERVIEW = "overview";
    public static final String POPULARITY = "popularity";
    public static final String VOTE_AVERAGE = "vote_average";


    public String id = "";
    public String poster_path = "";
    public String title = "";
    public String release_date = "";
    public ArrayList<Integer> genre_ids;
    public String overview = "";
    public double popularity = 0.0;
    public String vote_average = "";
    public boolean favourite;
    public float userRating;

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean getFavourite() {
        return favourite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Integer> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(ArrayList<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String description) {
        this.overview = description;
    }

    public String getVote_average() {
        return vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }

    public float getUserRating() {
        return userRating;
    }

    public void setUserRating(float userRating) {
        this.userRating = userRating;
    }

    public Movie() {

    }

    public static Movie fromJsonObject(JSONObject object) throws JSONException, ParseException {
        Movie movie = new Movie();

        movie.id = object.getString(MOVIE_ID);
        movie.title = object.getString(TITLE);
        movie.poster_path = object.getString(IMAGE);
        movie.release_date = object.getString(RELEASE_DATE);
        movie.genre_ids = fromJsonArrayToArray(object.getJSONArray(GENRES_ID));
        movie.overview = object.getString(OVERVIEW);
        movie.popularity = object.getDouble(POPULARITY);
        movie.vote_average = object.getString(VOTE_AVERAGE);

        return movie;
    }

    public static List<Movie> fromJsonArray(JSONArray jsonArray) throws JSONException, ParseException {

        List<Movie> movies = new ArrayList<Movie>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Movie movie = Movie.fromJsonObject(object);
            movies.add(movie);
        }
        return movies;
    }

    public static ArrayList<Integer> fromJsonArrayToArray(JSONArray jsonArray) throws JSONException, ParseException {

        ArrayList<Integer> genresArray = new ArrayList<Integer>();

        for (int i = 0; i < jsonArray.length(); i++) {
            genresArray.add(jsonArray.getInt(i));
        }
        return genresArray;
    }

}
