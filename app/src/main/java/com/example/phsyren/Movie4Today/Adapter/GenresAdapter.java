package com.example.phsyren.Movie4Today.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.phsyren.Movie4Today.Model.Genres;
import com.example.phsyren.Movie4Today.R;

import java.util.List;

/**
 * Created by Phsyren on 2016-03-30.
 */
public class GenresAdapter extends ArrayAdapter<Genres> {

    private final LayoutInflater layoutInflater;

    public GenresAdapter(Context context, int resource, List<Genres> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_genres, null);
            viewHolder.genres_name = (TextView) convertView.findViewById(R.id.genres_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Genres genres = getItem(position);
        viewHolder.genres_name.setText(genres.getName());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    private static class ViewHolder {

        public TextView genres_name;
    }
}
