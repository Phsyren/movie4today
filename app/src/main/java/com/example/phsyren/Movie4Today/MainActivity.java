package com.example.phsyren.Movie4Today;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.phsyren.Movie4Today.Adapter.GenresAdapter;
import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.AsyncTask.AsyncSaveGenres;
import com.example.phsyren.Movie4Today.AsyncTask.CurrentMoviesList;
import com.example.phsyren.Movie4Today.AsyncTask.NewMoviesList;
import com.example.phsyren.Movie4Today.Database.DAO;
import com.example.phsyren.Movie4Today.Database.Database;
import com.example.phsyren.Movie4Today.HTTP.UrlRequest;
import com.example.phsyren.Movie4Today.Model.Genres;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener {


    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ListView genresList;
    private SearchView searchView;
    private SearchManager searchManager;
    private Button popularBtn;
    private Button bestRatedBtn;
    private MoviesAdapter moviesAdapter;
    private GenresAdapter genresAdapter;
    private DAO dao;
    private Database database;
    String currentURL = UrlRequest.URL_API_POPULAR;
    private List<Genres> mGenres = new ArrayList<Genres>();
    private int page = 1;

    private int firstVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;

    MainActivity currentContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        database = new Database(getApplicationContext());

        dao = new DAO(getApplicationContext());

        genresAdapter = new GenresAdapter(this, R.layout.item_genres, mGenres);

        moviesAdapter = new MoviesAdapter(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(moviesAdapter);

        new CurrentMoviesList(this, moviesAdapter, page, recyclerView).execute(currentURL + page);


        popularBtn = (Button) findViewById(R.id.popularity);

        popularBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;

                currentURL = UrlRequest.URL_API_POPULAR;

                new NewMoviesList(currentContext, moviesAdapter, page, recyclerView).execute(currentURL + page);
                recyclerView.getLayoutManager().scrollToPosition(0);
                onBackPressed();
            }
        });

        bestRatedBtn = (Button) findViewById(R.id.top_rate);

        bestRatedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;

                currentURL = UrlRequest.URL_API_BEST_RATE;

                new NewMoviesList(currentContext, moviesAdapter, page, recyclerView).execute(currentURL + page);
                recyclerView.getLayoutManager().scrollToPosition(0);
                onBackPressed();
            }
        });

        final List<Genres> dbGenres = dao.getGenresFromDB();

        if (dbGenres.size() > 1) {
            genresAdapter.addAll(dbGenres);
        } else {
            new AsyncSaveGenres(this, genresAdapter).execute(UrlRequest.URL_API_GENRES_LIST);
        }

        genresList = (ListView) findViewById(R.id.genres_list);
        genresList.setAdapter(genresAdapter);

        genresList.setOnItemClickListener(this);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                                             @Override
                                             public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                 super.onScrolled(recyclerView, dx, dy);

                                                 if (dy > 0) {
                                                     visibleItemCount = layoutManager.getChildCount();
                                                     totalItemCount = layoutManager.getItemCount();
                                                     firstVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                                                     if (loading) {
                                                         if ((visibleItemCount + firstVisiblesItems) >= totalItemCount) {
                                                             loading = false;

                                                             page++;
                                                             new CurrentMoviesList(currentContext, moviesAdapter, page, recyclerView).execute(currentURL + page);
                                                             loading = true;
                                                         }
                                                     }

                                                 }
                                             }
                                         }
        );

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.mySearchView);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (query.length() > 1) {
                    page = 1;
                    currentURL = UrlRequest.URL_API_QUERY + query + UrlRequest.URL_API_PAGES;
                    new NewMoviesList(currentContext, moviesAdapter, page, recyclerView).execute(currentURL + page);

                }
                searchView.clearFocus();
                searchView.setQuery("", false);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Genres genres = mGenres.get(position);
        int genresId = genres.getId();
        page = 1;

        currentURL = "https://api.themoviedb.org/3/genre/" + genresId + "/movies?api_key=6d6169eb545767e4865955fc68731c23&language=pl&page=";

        new NewMoviesList(currentContext, moviesAdapter, page, recyclerView).execute(currentURL + page);
        recyclerView.getLayoutManager().scrollToPosition(0);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsMovie.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
